<!DOCTYPE html>
<html>
	<head>
		<title>Tentukan Nilai - Latihan 1</title>
	</head>
	<body>
		<?php
		// soal
		// function tentukan_nilai($number)
		// {
		//     //  kode disini
		// }
		//TEST CASES
		// echo tentukan_nilai(98); //Sangat Baik
		// echo tentukan_nilai(76); //Baik
		// echo tentukan_nilai(67); //Cukup
		// echo tentukan_nilai(43); //Kurang
		// jawaban
		//
		echo "<pre>";
			
		
		echo "<h3> Latihan Tentukan Nilai </h3>";
		function tentukan_nilai($number){
			if ($number >= 98) {
			echo 'Sangat Baik';
		} else if ($number < 77 && $number >= 68) {
			echo 'Baik';
		} else if ($number < 68 && $number >= 44) {
			echo 'Cukup';
		} else {
			echo 'Kurang';
		}
		}
		// echo tentukan_nilai();
		echo tentukan_nilai(98); //Sangat Baik
		echo "<br>";
		echo tentukan_nilai(76); //Baik
		echo "<br>";
		echo tentukan_nilai(67); //Cukup
		echo "<br>";
		echo tentukan_nilai(43); //Kurang
		echo"</pre>";
		?>
	</body>
</html>