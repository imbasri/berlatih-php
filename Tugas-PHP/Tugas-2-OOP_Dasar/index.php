<?php 

require_once ('ape.php');
require_once ('animal.php');
require_once ('frog.php');

$sheep = new Animal("Shaun");

echo "Nama Hewan : $sheep->name <br>";
echo "Jumlah Kaki : $sheep->legs <br>";
echo "Darah : $sheep->cold_blooded <br><br>";// false


$kodok = new Frog('Buduk');
echo "Nama Hewan : $kodok->name <br>";
echo "Jumlah Kaki : $kodok->legs <br>";
echo "Darah : $kodok->cold_blooded <br>";
echo $kodok->jump();
echo "<br><br>";



$sungokong = new Ape('Kera Sakti');
echo "Nama Hewan : $sungokong->name <br>";
echo "Jumlah Kaki : $sungokong->legs <br>";
echo "Darah : $sungokong->cold_blooded <br>";
echo $sungokong->yell();

// $kodok = new Animal('Buduk');
// echo "Nama Hewan : $kodok->name <br>";
// echo "Jumlah Kaki : $kodok->legs <br>";
// echo "Darah : $kodok->cold_blooded <br>";
 ?>