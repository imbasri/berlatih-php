<!DOCTYPE html>
<html>
	<head>
		<title>Besar Kecil</title>
	</head>
	<body>
		
		<?php
		// soal
		// function tukar_besar_kecil($string){
		//kode di sini}
		//
		//
		function tukar_besar_kecil($string)
	{
		$abjadkecil = "abcdefghijklmnopqrstuvwxyz";
		$output = "";
		for ($i = 0; $i < strlen($string); $i++) {
			
			$kecil = strpos($abjadkecil, $string[$i]);

			if ($kecil == null) {
				$output .= strtolower($string[$i]);
			} else {
				$output .= strtoupper($string[$i]);
			}
		}return 'hasil ubah : '.$output . "<br>";	
	}

		// TEST CASES
		echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
		echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
		echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
		echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
		echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"
		?>
		
	</body>
</html>